'use strict';
    
var Handlebars = window.Handlebars || {};

var source = $('#ticket-template').html(),
    template = Handlebars.compile(source),
    numbers = { tickets: [] };

for (var i = 0; i < 500; i++) {
  var arr = {
    num: i+1,
    lastPage: i % 4 === 3
  };
  numbers.tickets.push(arr);
}

$('.ticket-wrapper').html(template(numbers));
